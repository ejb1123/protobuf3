#
# spec file for package protobuf
#
# Copyright (c) 2016 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


%define soname 10
# disable check since it depends on lock gmock build
%bcond_with check
Name:           protobuf
Version:        3.0.0
Release:        0
Summary:        Protocol Buffers - Google's data interchange format
License:        BSD-3-Clause
Group:          Development/Libraries/C and C++
Url:            https://github.com/google/protobuf/
#Source0:        https://github.com/google/protobuf/releases/download/v%{version}/%{name}-%{version}.tar.bz2
Source0:        protobuf-3.0.0.tar.gz
Source2:        baselibs.conf
Patch0:         protobuf-setuptools-2.4.1.patch
# fix no-return-in-nonvoid-function google/protobuf/extension_set.cc:74
Patch1:         protobuf-return-no-nonvoid.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%if 0%{?fedora_version} > 8 || 0%{?mandriva_version} > 2008 || 0%{?suse_version} > 1030
# disabled java for 3.0beta
%bcond_with protobuf_java
%else
%bcond_with protobuf_java
%endif
%if 0%{?suse_version} < 1210
%bcond_with protobuf_python
%else
# disable python for 3.0 beta
%bcond_with protobuf_python
%endif
%if 0%{?suse_version} > 1010
BuildRequires:  fdupes
%endif
%if %{with protobuf_java}
BuildRequires:  java-devel >= 1.6.0
%endif
%if %{with protobuf_python}
BuildRequires:  python-dateutil
BuildRequires:  python-devel
BuildRequires:  python-gflags
BuildRequires:  python3-pytz
BuildRequires:  python-setuptools
BuildRequires:  zlib-devel
%endif
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  gcc-c++
BuildRequires:  libtool
BuildRequires:  pkgconfig, gmock-devel


%description
Protocol Buffers are a way of encoding structured data in an efficient yet
extensible format. Google uses Protocol Buffers for almost all of its internal
RPC protocols and file formats.

%package -n libprotobuf%{soname}
Summary:        Protocol Buffers - Google's data interchange format
Group:          System/Libraries

%description -n libprotobuf%{soname}
Protocol Buffers are a way of encoding structured data in an efficient yet
extensible format. Google uses Protocol Buffers for almost all of its internal
RPC protocols and file formats.

%package -n libprotoc%{soname}
Summary:        Protocol Buffers - Google's data interchange format
Group:          System/Libraries

%description -n libprotoc%{soname}
Protocol Buffers are a way of encoding structured data in an efficient yet
extensible format. Google uses Protocol Buffers for almost all of its internal
RPC protocols and file formats.

%package -n libprotobuf-lite%{soname}
Summary:        Protocol Buffers - Google's data interchange format
Group:          System/Libraries

%description -n libprotobuf-lite%{soname}
Protocol Buffers are a way of encoding structured data in an efficient yet
extensible format. Google uses Protocol Buffers for almost all of its internal
RPC protocols and file formats.

%package devel
Summary:        Header files, libraries and development documentation for %{name}
Group:          Development/Libraries/C and C++
Requires:       gcc-c++
Requires:       libprotobuf%{soname} = %{version}
Requires:       libprotobuf-lite%{soname}
Requires:       zlib-devel
Provides:       libprotobuf-devel = %{version}

%description devel
Protocol Buffers are a way of encoding structured data in an efficient yet
extensible format. Google uses Protocol Buffers for almost all of its internal
RPC protocols and file formats.

%if %{with protobuf_java}
%package -n %{name}-java
Summary:        Java Bindings for Google Protocol Buffers
Group:          Development/Libraries/Java
Requires:       java >= 1.6.0

%description -n %{name}-java
This package contains the Java bindings for Google Protocol Buffers.
%endif

%if %{with protobuf_python}
%package -n python-%{name}
Summary:        Python Bindings for Google Protocol Buffers
Group:          Development/Libraries/Python
%if 0%{?suse_version}
%py_requires
%else
Requires:       python
%endif

%description -n python-%{name}
This package contains the Python bindings for Google Protocol Buffers.
%endif

%prep

%setup
ls -a .
ls -a ..
#%patch0
#%patch1 -p1
#



%build
mkdir gmock
sh autogen.sh

%configure \
	--disable-static

make %{?_smp_mflags}

%if %{with protobuf_java}
pushd java
mkdir -p src/main/java
../src/protoc --java_out=src/main/java -I../src ../src/google/protobuf/descriptor.proto
mkdir classes
%if 0%{?suse_version} == 1110
# 11.1 only workaround
# http://en.opensuse.org/Java/Packaging/Cookbook#bytecode_version_error
extra_java_flags="-target 1.5 -source 1.5"
%endif
javac $extra_java_flags -d classes src/main/java/com/google/protobuf/*.java
sed -e 's/@VERSION@/%{version}/' < %{SOURCE1} > manifest.txt
jar cfm %{name}-java-%{version}.jar manifest.txt -C classes com
popd
%endif

%if %{with protobuf_python}
pushd python
python setup.py build
popd
%endif

%if %{with check}
%check
make %{?_smp_mflags} check
%endif

%install
make DESTDIR=%{buildroot} install %{?_smp_mflags}
install -Dm 0644 editors/proto.vim %{buildroot}%{_datadir}/vim/site/syntax/proto.vim
# no need for that
find %{buildroot} -type f -name "*.la" -delete -print

%if %{with protobuf_java}
pushd java
install -D -m 0644 %{name}-java-%{version}.jar %{buildroot}%{_javadir}/%{name}-java-%{version}.jar
ln -s %{name}-java-%{version}.jar %{buildroot}%{_javadir}/%{name}-java.jar
ln -s %{name}-java-%{version}.jar %{buildroot}%{_javadir}/%{name}.jar
popd
%endif

%if %{with protobuf_python}
pushd python
python setup.py install --skip-build \
	--prefix=%{_prefix} \
	--install-data=%{_datadir} \
	--root %{buildroot} \
	--record-rpm=INSTALLED_FILES
popd
%endif

%if 0%{?suse_version} > 1010
%fdupes -s %{buildroot}%{py_sitedir}
%endif

%post -n libprotobuf%{soname} -p /sbin/ldconfig

%postun -n libprotobuf%{soname} -p /sbin/ldconfig

%post -n libprotoc%{soname} -p /sbin/ldconfig

%postun -n libprotoc%{soname} -p /sbin/ldconfig

%post -n libprotobuf-lite%{soname} -p /sbin/ldconfig

%postun -n libprotobuf-lite%{soname} -p /sbin/ldconfig

%files -n libprotobuf%{soname}
%defattr(-, root, root)
%{_libdir}/libprotobuf.so.%{soname}*

%files -n libprotoc%{soname}
%defattr(-, root, root)
%{_libdir}/libprotoc.so.%{soname}*

%files -n libprotobuf-lite%{soname}
%defattr(-, root, root)
%{_libdir}/libprotobuf-lite.so.%{soname}*

%files devel
%defattr(-,root,root)
%doc CHANGES.txt CONTRIBUTORS.txt README.md
%{_bindir}/protoc
%{_includedir}/google
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_datadir}/vim

%if %{with protobuf_java}
%files -n %{name}-java
%defattr(-,root,root)
%{_javadir}/protobuf*
%endif

%if %{with protobuf_python}
%files -n python-%{name} -f python/INSTALLED_FILES
%defattr(-,root,root)
%endif

%changelog
